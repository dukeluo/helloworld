package com.thoughtworks.hello;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import  java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class Controller {

    @GetMapping("/offices")
    public List<Office> getOffices() {
        List<Office> offices = new ArrayList<Office>();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con=DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/jap-exercise?useSSL=false","root","test");
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery("select id, city from offices");
            while(rs.next()) {
                int id = rs.getInt("id");
//                int id = rs.getInt(1);
                String city = rs.getString("city");
                offices.add(new Office(id, city));
            }
            con.close();
        } catch(Exception e) {
            System.out.println(e);
        }
        return offices;
    }

}

package com.thoughtworks.hello;

public class Office {
    private int id;
    private String city;

    public Office(int id, String city) {
        this.id = id;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }
}
